#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
from isnlila.lila.models import *


sr_id = 41
date = datetime.date(2015, 4, 13)

stop_route = StopRoute.objects.select_related().get(id=sr_id)
route_id = stop_route.route.route_id
stop_id = stop_route.stop.stop_id
direction_id = stop_route.direction_id


days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']

f = {'start_date__lte':date, 'end_date__gte':date, days[date.weekday()]: 1}
services = set(Calendar.objects.filter(**f).values_list('service_id', flat=True))

for c in CalendarDate.objects.filter(date=date, exception_type=1):
    services.add(c.service_id)
for c in CalendarDate.objects.filter(date=date, exception_type=2):
    services.discard(c.service_id)



st = StopTime.objects.filter(
        trip__service_id__in=services,
        trip__route__route_id=route_id,
        trip__direction_id=direction_id,
        stop_id=stop_id
    )
print st.query
print [x.arrival_time for x in st]



