.mode csv

.import gtfs_sql/agency.csv agency
.import gtfs_sql/calendar_dates.csv calendar_dates
.import gtfs_sql/calendar.csv calendar
.import gtfs_sql/routes.csv routes
.import gtfs_sql/stops.csv stops
.import gtfs_sql/stop_times.csv stop_times
.import gtfs_sql/trips.csv trips
