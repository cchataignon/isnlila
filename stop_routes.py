#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3

conn = sqlite3.connect('db.sqlite3')
c = conn.cursor()
for r in c.execute("select * from stop_routes"):
    r = list(r)
    term = 'SELECT city FROM stops WHERE stop_id IN (select distinct stop_id from stop_times where trip_id in (select trip_id from trips where route_id="%s") order by arrival_time DESC limit 1);' % r[2]
    c2 = conn.cursor()
    res_term = c2.execute(term).fetchone()[0]
    c3 = conn.cursor()
    c3.execute('update stop_routes set terminus="%s" where route_id="%s"' % (res_term, r[2]))

conn.commit()
conn.close()

