#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import operator
import csv
import sys
import os
from PIL import Image


def get_color(img):
    im = Image.open(img)
    rgb_im = im.convert("RGB")
    w, h = im.size
    dic = {}

    for y in range(h):
        for x in range(w):
            pixel = rgb_im.getpixel((x, y))
            if pixel in dic:
                dic[pixel] += 1
            else:
                dic[pixel] = 0

    return "#%02x%02x%02x" % max(dic.iteritems(), key=operator.itemgetter(1))[0]


def main():
    with open(sys.argv[1]) as f:
        reader = csv.reader(f)
        for r in reader:
            path = "assets/img/%s.png" % r[2]
            if os.path.isfile(path):
                r[7] = get_color(path)
            else:
                r[7] = "#000000"

            print (*r, sep=",")




if __name__ == "__main__":
    main()
