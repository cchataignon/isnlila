#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import

import csv
import sys

def main():
    with open(sys.argv[1]) as f:
        reader = csv.reader(f)
        ids = set()
        for i, r in enumerate(reader):
            if len(r) == 5:
                r.append(0)
            if r[2] not in ids:
                print(*r, sep=",")
            ids.add(r[2])



if __name__ == '__main__':
    main()

