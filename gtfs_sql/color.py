#!/usr/bin/env python
# -*- coding: utf-8 -*-

import operator
from PIL import Image

def get_color(img):
    im = Image.open("img/%s.png" % img)
    w, h = im.size
    dic = {}

    for y in range(h):
        for x in range(w):
            pixel = im.getpixel((x, y))
            if pixel in dic:
                dic[pixel] += 1
            else:
                dic[pixel] = 0

    return max(dic.iteritems(), key=operator.itemgetter(1))[0]
