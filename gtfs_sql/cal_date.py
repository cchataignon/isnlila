#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import
from datetime import datetime

import csv
import sys

def main():
    with open(sys.argv[1]) as f:
        reader = csv.reader(f)
        next(reader)
        for i, r in enumerate(reader):
            date = datetime.strptime(r[1], "%Y%m%d")
            r[1] = date.strftime("%Y-%m-%d")
            print(i, *r, sep=",")



if __name__ == '__main__':
    main()

