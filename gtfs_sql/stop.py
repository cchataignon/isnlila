#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import

import csv
import sys
import os

reload(sys)
sys.setdefaultencoding("utf-8")

def main():
    with open(sys.argv[1]) as f:
        reader = csv.reader(f)
        ids = set()
        next(reader)
        if not os.path.isfile("gtfs_sql/city.csv"):
            import reverse_geocoder as rg
            for i, r in enumerate(reader):
                coord = tuple(map(float, r[3:5]))
                cit = rg.get(coord)['name']
                print(cit)
        else:
            with open("gtfs_sql/city.csv") as c:
                reader_city = csv.reader(c)
                l_reader = [r for r in reader]
                for i, r in enumerate(reader_city):
                    l_reader[i].insert(2, r[0])
                    print(*l_reader[i], sep=",")


if __name__ == '__main__':
    main()

