#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import

import csv
import sys

def main():
    with open(sys.argv[1]) as f:
        reader = csv.reader(f)
        for i, r in enumerate(reader):
            print(i, *r, sep=",")



if __name__ == '__main__':
    main()

