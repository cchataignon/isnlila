#!/bin/zsh

sed '1d' gtfs_sql/agency.txt > gtfs_sql/agency.csv
python gtfs_sql/cal_date.py gtfs_sql/calendar_dates.txt > gtfs_sql/calendar_dates.csv
python gtfs_sql/cal.py gtfs_sql/calendar.txt > gtfs_sql/calendar.csv
python gtfs_sql/routes.py gtfs_sql/routes.txt | sed '1d' > gtfs_sql/routes.csv
python gtfs_sql/stop.py gtfs_sql/stops.txt | sed '1d' > gtfs_sql/stops.csv
python gtfs_sql/add_pk.py gtfs_sql/stop_times.txt | sed '1d' > gtfs_sql/stop_times.csv
python gtfs_sql/trip.py gtfs_sql/trips.txt | sed '1d' > gtfs_sql/trips.csv

sqlite3 db.sqlite3 < populate.sql
sqlite3 db.sqlite3 < stop_route.sql
python stop_routes.py
