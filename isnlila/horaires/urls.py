#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from isnlila.horaires import views

urlpatterns = patterns('',
        url(r'^view/(?P<sr_id>\d+)/$', views.view, name='horaires.view'),
        url(r'^trip/(?P<trip_id>.+)/$', views.trip, name='horaires.trip'),
)

