from django.shortcuts import render
from django.http import JsonResponse

from isnlila.horaires.forms import DateForm
from isnlila.lila.models import StopRoute, StopTime, CalendarDate, Calendar, Trip
from datetime import datetime, date, time

def view(request, sr_id):
    stop_route = StopRoute.objects.select_related().get(id=sr_id)
    route_id = stop_route.route.route_id
    stop_id = stop_route.stop.stop_id
    direction_id = stop_route.direction_id
    stop_times = []
    date = None

    if request.method == 'GET':
        form = DateForm()
    else:
        form = DateForm(request.POST)
        if form.is_valid():
            date = datetime.strptime(form.cleaned_data['date'], '%d/%m/%Y')
            days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']

            f = {'start_date__lte':date, 'end_date__gte':date, days[date.weekday()]: 1}
            services = set(Calendar.objects.filter(**f).values_list('service_id', flat=True))

            for c in CalendarDate.objects.filter(date=date, exception_type=1):
                services.add(c.service_id)
            for c in CalendarDate.objects.filter(date=date, exception_type=2):
                services.discard(c.service_id)

            st = StopTime.objects.filter(
                    trip__service_id__in=services,
                    trip__route__route_id=route_id,
                    trip__direction_id=direction_id,
                    stop_id=stop_id
                ).order_by('arrival_time')


            arrivals = set()
            stop_times = [s for s in st if not (s.arrival_time in arrivals or arrivals.add(s.arrival_time))]

    return render(request, 'horaires/view.html', {
        'sr_id': sr_id,
        'stop_route': stop_route,
        'stop_times': stop_times,
        'form' : form,
        'date': date
        }
    )


def trip(request, trip_id):
    st = StopTime.objects.filter(trip__trip_id=trip_id)

    arrivals = set()
    stop_times = [s for s in st if not (s.arrival_time in arrivals or arrivals.add(s.arrival_time))]

    return render(request, 'horaires/trip.html', {
        'stop_times': stop_times
        }
    )


