from django import forms
from datetime import date

class DateForm(forms.Form) :
    date = forms.CharField(required=True, widget=forms.TextInput(attrs={'class':'datepicker', 'autocomplete': 'off', 'value': date.today().strftime('%d/%m/%Y')}))
