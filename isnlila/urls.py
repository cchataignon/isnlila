from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import RedirectView

urlpatterns = patterns('',
    # Examples:
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', RedirectView.as_view(pattern_name='search.stop', permanent=False)),
    url(r'^horaires/', include('isnlila.horaires.urls')),
    url(r'^ligne/', include('isnlila.ligne.urls')),
    url(r'^search/', include('isnlila.search.urls')),
    url(r'^carte/', include('isnlila.carte.urls')),
)
