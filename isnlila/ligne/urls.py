#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from isnlila.ligne import views

urlpatterns = patterns('',
        url(r'^all/$', views.all, name='ligne.all'),
        url(r'^(?P<short_name>.+)/$', views.ligne, name='ligne.ligne'),
)

