from django.shortcuts import render
from django.http import JsonResponse

from isnlila.lila.models import Route, StopRoute
from isnlila.ligne.forms import StopLigneForm

def all(request):
    routes = Route.objects.all()
    short_names = set()
    routes = [r for r in routes if not (r.short_name in short_names or short_names.add(r.short_name))]
    return render(request, 'lignes/index.html', {
        'routes': routes
        }
    )

def ligne(request, short_name):
    stop_routes = []
    if request.method == 'GET':
        form = StopLigneForm(short_name=short_name)
    else:
        form = StopLigneForm(request.POST, short_name=short_name)
        if form.is_valid():
            route_id = form.cleaned_data['sens']
            stop_routes = StopRoute.objects.filter(route__route_id=route_id).order_by('stop__name')

    return render(request, 'lignes/ligne.html', {
        'form': form,
        'short_name': short_name,
        'stop_routes': stop_routes,
        }
    )
