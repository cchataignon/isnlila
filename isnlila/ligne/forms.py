#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django import forms

from isnlila.lila.models import StopRoute

class StopLigneForm(forms.Form) :
    sens = forms.ChoiceField(required=False)
    def __init__(self, *args, **kwargs):
        short_name = kwargs.pop('short_name')
        super(StopLigneForm, self).__init__(*args, **kwargs)
        self.fields['sens'].choices = StopRoute.objects.filter(route__short_name=short_name).values_list('route__route_id', 'terminus').distinct()
