from django.shortcuts import render
from django.http import JsonResponse
from isnlila.lila.models import StopRoute

def index(request):
    return render(request, 'carte/index.html', {})

def get_coords(request):
    stops = StopRoute.objects.all().exclude(stop__lat=0, stop__lon=0).values(
            'id',
            'stop__name',
            'stop__lat',
            'stop__lon',
            'route__color')

    d = {s['id']:
            {
                'name': s['stop__name'],
                'lat': s['stop__lat'],
                'lon': s['stop__lon'],
                'color': s['route__color']
            }
        for s in stops}

    return JsonResponse(d)
