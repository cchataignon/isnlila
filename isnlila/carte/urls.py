#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from isnlila.carte import views

urlpatterns = patterns('',
        url(r'^$', views.index, name='carte.index'),
        url(r'^get_coords/$', views.get_coords, name='carte.get_coords'),
)

