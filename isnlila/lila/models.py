from django.db import models

# Create your models here.

class Agency(models.Model):
    agency_id = models.CharField(max_length=255, blank=True, primary_key=True)
    name = models.CharField(max_length=255)
    url = models.CharField(max_length=255, blank=True)
    timezone = models.CharField(max_length=255)
    lang = models.CharField(max_length=255)

    class Meta:
        db_table = "agency"

class Route(models.Model):
    route_id = models.CharField(max_length=255, primary_key=True)
    agency = models.ForeignKey(Agency, null=True, blank=True)
    short_name = models.CharField(max_length=255)
    long_name = models.TextField()
    desc = models.TextField(null=True, blank=True)
    route_type = models.IntegerField(default=3)
    url = models.URLField(null=True, blank=True)
    color = models.CharField(max_length=6, default="FFFFFF")
    text_color = models.CharField(max_length=6, default="000000")

    class Meta:
        db_table = "routes"

class CalendarDate(models.Model):
    service_id = models.IntegerField()
    date = models.DateField()
    exception_type = models.IntegerField()

    class Meta:
        db_table = "calendar_dates"

class Calendar(models.Model):
    service_id = models.IntegerField()
    monday = models.IntegerField()
    tuesday = models.IntegerField()
    wednesday = models.IntegerField()
    thursday = models.IntegerField()
    friday = models.IntegerField()
    saturday = models.IntegerField()
    sunday = models.IntegerField()
    start_date = models.DateField()
    end_date = models.DateField()

    class Meta:
        db_table = "calendar"

class Stop(models.Model):
    stop_id = models.CharField(max_length=255, primary_key=True)
    name = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    desc = models.TextField(null=True, blank=True)
    lat = models.FloatField()
    lon = models.FloatField()
    zone_id = models.TextField(null=True, blank=True)
    url = models.URLField(null=True, blank=True)
    location_type = models.IntegerField(default=1)
    parent_station = models.ForeignKey('self', null=True, blank=True)

    class Meta:
        db_table = "stops"

class Trip(models.Model):
    route = models.ForeignKey(Route)
    service_id = models.IntegerField()
    trip_id = models.CharField(max_length=255, primary_key=True)
    headsign = models.CharField(max_length=255)
    direction_id = models.IntegerField()
    block_id = models.IntegerField(blank=True, default=0)

    class Meta:
        db_table = "trips"

class StopTime(models.Model):
    trip = models.ForeignKey(Trip)
    arrival_time = models.TimeField()
    departure_time = models.TimeField()
    stop = models.ForeignKey(Stop)
    stop_sequence = models.IntegerField()
    headsign = models.CharField(max_length=255)
    pickup_type = models.IntegerField()
    drop_off_type = models.IntegerField()

    class Meta:
        db_table = "stop_times"


class StopRoute(models.Model):
    stop = models.ForeignKey(Stop)
    route = models.ForeignKey(Route)
    direction_id = models.IntegerField()
    long_name = models.CharField(max_length=255)
    terminus = models.CharField(max_length=255)

    class Meta:
        db_table = "stop_routes"


