from django.shortcuts import render
from django.http import JsonResponse

from isnlila.search.forms import SearchStopForm
from isnlila.lila.models import StopRoute

def search_stop(request):
    stop_routes = []
    if request.method == 'GET':
        form = SearchStopForm()
    else:
        form = SearchStopForm(request.POST)
        if form.is_valid():
            long_name = form.cleaned_data['long_name']
            stop_routes = StopRoute.objects.filter(long_name=long_name).select_related()

    return render(request, 'search/stop.html', {
        'form': form,
        'stop_routes': stop_routes,
        }
    )

# JSON

def suggest_stop(request, search):
    stop_routes = [{
        'long_name': "%s|%s" % (s['stop__name'], s['stop__city'])
        } for s in StopRoute.objects
                        .filter(long_name__contains=search)
                        .values('stop__name', 'stop__city')
                        .distinct()
                        .order_by('stop__name', 'stop__city')
    ]
    return JsonResponse({"results": stop_routes})
