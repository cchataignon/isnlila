#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url
from isnlila.search import views

urlpatterns = patterns('',
        url(r'^stop/$', views.search_stop, name='search.stop'),
        url(r'^suggest/stop/(?P<search>[\w\W]+)/$', views.suggest_stop, name='search.suggest_stop'),
)

