from django import forms

class SearchStopForm(forms.Form) :
    stop = forms.CharField(required=False, widget=forms.TextInput(attrs={'autocomplete': 'off'}))
    long_name = forms.CharField(required=True, widget=forms.HiddenInput())
