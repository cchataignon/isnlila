drop table stop_routes;

create table stop_routes(
    "id" integer primary key autoincrement,
    "stop_id" varchar(255) not null,
    "route_id" varchar(255) not null references "trips" ("route_id"),
    "direction_id" varchar(255) not null references "trips" ("direction_id"),
    "long_name" varchar(255),
    "terminus" varchar(255)

);

insert into stop_routes (stop_id, route_id, direction_id, long_name) select distinct stop_id, route_id, direction_id, name ||' '|| city from stops join stop_times using (stop_id) join trips using (trip_id);

